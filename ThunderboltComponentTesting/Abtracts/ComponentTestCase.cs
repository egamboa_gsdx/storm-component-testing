﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Essy.Thunderbolt;
using Essy.Thunderbolt.LowLevel;

namespace ThunderboltComponentTesting.Abtracts
{
    abstract class ComponentTestCase
    {
        public abstract string TestName { get; }
        public abstract void RunTest(InstrumentBase instrument);
    }
}
