﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Threading;
using System.Reflection;
using System.IO;
using Essy.Thunderbolt;
using Essy.Thunderbolt.LowLevel;
using Essy.EssyBus;
using Essy.EssyBusBLDCMotorController;
using ThunderboltComponentTesting.Abtracts;
using ThunderboltComponentTesting.Helpers;
using ThunderboltComponentTesting.Interfaces;
using PropertyChanged;

namespace ThunderboltComponentTesting
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [AddINotifyPropertyChangedInterface]
    public partial class MainWindow : Window, IStatusHelper
    {
        public static string ProgramLocalAppData => DirectoryExists(_programLocalAppData);
        private static string _programLocalAppData => Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
            Path.DirectorySeparatorChar + "GSD Test Lab" + Path.DirectorySeparatorChar + "Component Testing";
        private InstrumentBase _instrument;
        private SortedDictionary<string, ComponentTestCase> _compontentTests = GetTests<ComponentTestCase>();
        private BackgroundWorker _worker = new BackgroundWorker();
        private ComponentTestCase _testToRun;
        public MainWindow()
        {
            InitializeComponent();
            StatusHelper.RegisterHelper(this);
            try
            {
                var isConnected = _instrument.GetType().Name;
            }
            catch
            {
                try
                {
                    var version = new Version();
                    _instrument = TBFactory.CreateThunderBolt(false, true, out version, null);


                }
                catch (Exception ex)
                {
                    MessageBox.Show($"{ex}");
                }
            }
            finally
            {
                if (_instrument == null)
                {
                    MessageBox.Show("Could not connect with any instrument.");
                }
                else
                {
                    instrName.Text = _instrument.InstrumentName;
                    instrID.Text = _instrument.InstrumentSerialNumber;
                    instrVersion.Text = _instrument.InstrumentHardwareVersion.ToString();
                    foreach (var test in _compontentTests)
                    {
                        TestListBox.Items.Add(test.Key);
                    }
                }
            }

            _worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
            _worker.RunWorkerCompleted += Worker_RunCompleted;
            _worker.ProgressChanged += Worker_ProgressChanged;
            _worker.WorkerReportsProgress = true;
        }

        private static SortedDictionary<string, ComponentTestCase> GetTests<T>() where T : ComponentTestCase
        {
            var tests = new SortedDictionary<string, ComponentTestCase>();

            foreach (Type type in Assembly.GetCallingAssembly().GetTypes())
            {
                if (type.BaseType == typeof(T) && !type.IsAbstract)
                {
                    T testCaseInstance = (T)Activator.CreateInstance(type);
                    tests.Add(testCaseInstance.TestName, testCaseInstance);
                }
            }
            return tests;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _testToRun = _compontentTests[TestListBox.SelectedItem.ToString()];
            TestStatus.Clear();
            _worker.RunWorkerAsync();
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            TestStatus.Text += e.UserState as string;
        }

        private void Worker_RunCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string csvDirectory = ProgramLocalAppData + Path.DirectorySeparatorChar + "Thunderbolt";
            if (!Directory.Exists(csvDirectory))
            {
                Directory.CreateDirectory(csvDirectory);
            }
            string csvFileName =  csvDirectory + Path.DirectorySeparatorChar + "thunderbolt_component_testing.csv";
            if (!File.Exists(csvFileName))
            {
                File.Create(csvFileName);
                string headers = "Test Name" + "," + "Start Time" + "," +
                    "End Time" + "," + "Pass/Fail" + "," + "Error(if any)" + Environment.NewLine;
                File.WriteAllText(csvFileName, headers);
            }
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            OnStatusAdd($"Starting: {_testToRun.TestName}");
            _testToRun.RunTest(_instrument);
        }

        public void OnStatusAdd(string status)
        {
            _worker.ReportProgress(0, "\n" + status);
        }

        private void TestStatus_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            
        }

        private static string DirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }
    }
}
