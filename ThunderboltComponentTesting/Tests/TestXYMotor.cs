﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ThunderboltComponentTesting.Abtracts;
using ThunderboltComponentTesting.Helpers;
using Essy.Thunderbolt;
using Essy.Thunderbolt.LowLevel;
using Essy.EssyBusBLDCMotorController;

namespace ThunderboltComponentTesting.Tests
{
    class TestXYMotor : ComponentTestCase
    {
        public override string TestName
        {
            get
            {
                return "Test X and Y Motor";
            }
        }

        public override void RunTest(InstrumentBase instrument)
        {
            try
            {
                var follwerDevices = instrument.GetAllFollowerDevices();

                var xBLDC = follwerDevices.FirstOrDefault(x => x.Name.Equals("X-BLDC Motor")) as EssyBusBLDCMotorController;
                var yBLDC = follwerDevices.FirstOrDefault(y => y.Name.Equals("Y-BLDC Motor")) as EssyBusBLDCMotorController;

                // Initialize X-BLDC Motor
                xBLDC.Position = 0;
                xBLDC.P = 600;
                xBLDC.PIdle = 600;
                xBLDC.I = 2;
                xBLDC.D = 150;
                xBLDC.Jerk = (float)0.007;
                xBLDC.SettleWindow = 250;
                xBLDC.EarlyArrivalDelta = 20;
                xBLDC.ApplySettings();
                StatusHelper.AddStatus("BLDC X Success!");

                // Initialize Y-BLDC Motor
                yBLDC.Position = 0;
                yBLDC.P = 600;
                yBLDC.PIdle = 600;
                yBLDC.I = 2;
                yBLDC.D = 150;
                yBLDC.Jerk = (float)0.007;
                yBLDC.SettleWindow = 250;
                yBLDC.EarlyArrivalDelta = 20;
                yBLDC.ApplySettings();
                StatusHelper.AddStatus("BLDC Y Success!\n\n");

                StatusHelper.AddStatus("Y-Stepper motor homing");
                var yStopBack = new Essy.Motor.MotorStopChannel();
                yStopBack.StopMode = 0;
                yStopBack.StopAtHighLevel = false;
                yBLDC.SetMotorStopChannel1(yStopBack);

                yBLDC.Powered = true;
                yBLDC.MoveCounterClockWiseUntilStopChannel(1024, 2048, 1);
                yBLDC.WaitUntilStopCondition();
                yBLDC.Powered = false;
                yBLDC.Position = 0;

                StatusHelper.AddStatus("X-Stepper motor homing");
                var xStopBack = new Essy.Motor.MotorStopChannel();
                xStopBack.StopMode = 0;
                xStopBack.StopAtHighLevel = false;
                xBLDC.SetMotorStopChannel1(xStopBack);

                xBLDC.Powered = true;
                xBLDC.MoveCounterClockWiseUntilStopChannel(1024, 2048, 1);
                xBLDC.WaitUntilStopCondition();
                xBLDC.Powered = false;
                xBLDC.Position = 0;

                StatusHelper.AddStatus("Done homing!");

                xBLDC.Powered = true;
                yBLDC.Powered = true;
                for (int i = 0; i < 5; i++)
                {
                    StatusHelper.AddStatus($"Cycles Left: {5 - i}");
                    xBLDC.Powered = true;
                    xBLDC.MoveToPosition(16500, 65535, 35000, 65535);
                    xBLDC.Powered = false;
                    Thread.Sleep(2000);
                    yBLDC.Powered = true;
                    yBLDC.MoveToPosition(14000, 65535, 35000, 65535);
                    yBLDC.Powered = false;
                    Thread.Sleep(2000);
                    xBLDC.Powered = true;
                    xBLDC.MoveToPosition(50, 65535, 35000, 65535);
                    xBLDC.Powered = false;
                    Thread.Sleep(2000);
                    yBLDC.Powered = true;
                    yBLDC.MoveToPosition(50, 65535, 35000, 65535);
                    yBLDC.Powered = false;
                    Thread.Sleep(2000);
                }

                StatusHelper.AddStatus("Completed!");
            }
            catch (Exception ex)
            {
                StatusHelper.AddStatus($"Exception!\n\n{ex}");
            }
        }
    }
}
