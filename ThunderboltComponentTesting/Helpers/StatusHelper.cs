﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.IO;
using ThunderboltComponentTesting.Interfaces;

namespace ThunderboltComponentTesting.Helpers
{
    public class StatusHelper
    {
        private static List<IStatusHelper> _helpers = new List<IStatusHelper>();
        public static void RegisterHelper(IStatusHelper statusHelper)
        {
            _helpers.Add(statusHelper);
        }

        public static void AddStatus(string status)
        {
            foreach(IStatusHelper helper in _helpers)
            {
                try
                {
                    helper.OnStatusAdd(status);
                }
                catch
                {

                }
            }
        }
    }
}
