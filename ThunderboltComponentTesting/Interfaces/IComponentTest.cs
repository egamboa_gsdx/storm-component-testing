﻿using Essy.Thunderbolt;
using Essy.Thunderbolt.LowLevel;

namespace ThunderboltComponentTesting.Interfaces
{
    interface IComponentTest
    {
        string TestName { get; }
        void RunTest(InstrumentBase instrument);
    }
}
